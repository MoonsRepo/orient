import random

etudiants = [
    {
        'name': 'Mounir',
        'matricule': '171736003461',
    },
    {
        'name': 'Assem',
        'matricule': '171835027186',
    },
    {
        'name': 'Djouss',
        'matricule': '1718212121',
    },


]

specialities = [
    {
        'name': 'SII',
    },
    {
        'name': 'RSD',
    },
    {
        'name': 'IL',
    },
    {
        'name':'MIV'
    },
    {
        'name':'SSI'
    },
    {
        'name':'BIGDATAA'
    },
    {
        'name':'BIOINFO'
    },

]

modules = [
    {
        'name': 'Archi 01',
    },
    {
        'name': 'Algo',
    },
    {
        'name': 'SI',
    },
    {
        'name':'Analyse Numérique'
    },
    {
        'name':'Proba Stat'
    },
    {
        'name':'Logique.M'
    },
    {
        'name':'anglais'
    },
    {
        'name':'POO'
    },
    {
        'name':'BDD'
    },
    {
        'name':'Théorie des languages'
    },
    {
        'name':'SE 01'
    },
    {
        'name':'Archi02'
    },
    {
        'name':'SE 02'
    },
    {
        'name':'Réseau'
    },
    {
        'name':'Compilation'
    },
    {
        'name':'GL'
    },
    {
        'name': 'THG'
    },
    {
        'name': 'GL'
    },

]

moyennes = [

    {

        'etudiant_id': 1,
        'module_id': 1,
        'moyenne': round(random.uniform(4, 16), 2),
    },
    {

        'etudiant_id': 1,
        'module_id': 2,
        'moyenne': round(random.uniform(4, 16), 2),
    },
    {

        'etudiant_id': 1,
        'module_id': 3,
        'moyenne': round(random.uniform(4, 16), 2),
    },
    {

        'etudiant_id': 1,
        'module_id': 4,
        'moyenne': round(random.uniform(4, 16), 2),
    },
    {

        'etudiant_id': 1,
        'module_id': 5,
        'moyenne': round(random.uniform(4, 16), 2),
    },
    {

        'etudiant_id': 1,
        'module_id': 6,
        'moyenne': round(random.uniform(4, 16), 2),
    },
    {

        'etudiant_id': 1,
        'module_id': 7,
        'moyenne': round(random.uniform(4, 16), 2),
    },
    {

        'etudiant_id': 1,
        'module_id': 8,
        'moyenne': round(random.uniform(4, 16), 2),
    },
    {

        'etudiant_id': 1,
        'module_id': 9,
        'moyenne': round(random.uniform(4, 16), 2),
    },
    {

        'etudiant_id': 1,
        'module_id': 10,
        'moyenne': round(random.uniform(4, 16), 2),
    },
    {

        'etudiant_id': 1,
        'module_id': 11,
        'moyenne': round(random.uniform(4, 16), 2),
    },
    {

        'etudiant_id': 1,
        'module_id': 12,
        'moyenne': round(random.uniform(4, 16), 2),
    },
    {

        'etudiant_id': 1,
        'module_id': 13,
        'moyenne': round(random.uniform(4, 16), 2),
    },
    {

        'etudiant_id': 1,
        'module_id': 14,
        'moyenne': round(random.uniform(4, 16), 2),
    },
    {

        'etudiant_id': 1,
        'module_id': 15,
        'moyenne': round(random.uniform(4, 16), 2),
    },
    {

        'etudiant_id': 1,
        'module_id': 16,
        'moyenne': round(random.uniform(4, 16), 2),
    },
    {

        'etudiant_id': 1,
        'module_id': 17,
        'moyenne': round(random.uniform(4, 16), 2),
    },

]





